<?php include 'header.php'; ?>
<?php
include 'koneksi.php';
$db = new database(); 
?>

<h3><span class="glyphicon glyphicon-user"></span>  Daftar Pesanan</h3>
<button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span>Tambah </button>
<br/>
<br/>
<div>
	<a style="margin-bottom:10px" href="" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
</div>
<form action="cari_act.php" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari user di sini .." aria-describedby="basic-addon1" name="cari">	
	</div>
</form>
<br/>
<table class="table table-hover">
    <thead>
  <tr>
		<th>NO</th>
		<th>Nomor Meja</th>
		<th>Menu</th>
		<th>Harga</th>
		<th>Total</th>
		<th>Tindakan</th>
  </tr>
  </thead>
  <tbody>
  <?php
   $no = 1;
  foreach ($db->tampil_pesanan() as $data) { ?>
   <tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['nomer_meja']; ?></td>
			<td><?php echo $data['nama_menu']; ?></td>
			<td><?php echo $data['harga']; ?></td>
			<td><?php echo $data['total']; ?></td>
			<td>
				<a href="menu_det.php?id_pesanan=<?php echo $data['id_pesanan']; ?>" class="btn btn-info">Detail</a>
				<a href="menu_edit.php?id_pesanan=<?php echo $data['id_pesanan']; ?>" class="btn btn-warning">Edit</a>
				<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='proses.php?id_pesanan=<?php echo $data['id_pesanan']; ?>&aksi=hapus3' }" class="btn btn-danger">Hapus</a>
			</td>
		</tr>
    </tbody>
    <?php
  }
  ?>
</table>
<!-- modal input -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Pesanan Baru</h4>
			</div>
			<div class="modal-body">
				<form action="proses.php?aksi=tambah3" method="post">
					<div class="form-group">
						<label>Nomor Meja</label>
						<input name="nomer_meja" type="text" class="form-control" placeholder="Nomor Meja">
					</div>
					<div class="form-group">
						<label>Pilih Menu</label>
						<input name="nama_Menu" type="text" class="form-control" placeholder="Menu">
					</div>
					<div class="form-group">
						<label>Harga</label>
						<input name="harga" type="text" class="form-control" placeholder="Harga">
					</div>
					<div class="form-group">
						<label>Total</label>
						<input name="total" type="text-area" class="form-control" placeholder="Total">
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>
