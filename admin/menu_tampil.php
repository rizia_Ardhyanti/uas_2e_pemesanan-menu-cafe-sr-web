<?php include 'header.php'; ?>
<?php
include 'koneksi.php';
$db = new database(); 
?>

<h3><span class="glyphicon glyphicon-user"></span>  Daftar Menu</h3>
<button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span>Tambah </button>
<br/>
<br/>
<div>
	<a style="margin-bottom:10px" href="" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
</div>
<form action="cari_act.php" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari user di sini .." aria-describedby="basic-addon1" name="cari">	
	</div>
</form>
<br/>
<table class="table table-hover">
    <thead>
  <tr>
		<th>NO</th>
		<th>Nama Menu</th>
		<th>Kategori</th>
		<th>Photos</th>
		<th>Harga</th>
		<th>Tindakan</th>
  </tr>
  </thead>
  <tbody>
  <?php
   $no = 1;
  foreach ($db->tampil_menu() as $data) { ?>
   <tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['nama_menu']; ?></td>
			<td><?php echo $data['nama_kategori']; ?></td>
			<td align="center"><?php echo "<img src='../images/$data[photos]' width='90' height='90' />";?></td>
			<td><?php echo $data['harga']; ?></td>
			<td>
				<a href="menu_det.php?id_menu=<?php echo $data['id_menu']; ?>" class="btn btn-info">Detail</a>
				<a href="menu_edit.php?id_menu=<?php echo $data['id_menu']; ?>" class="btn btn-warning">Edit</a>
				<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='proses.php?id_menu=<?php echo $data['id_menu']; ?>&aksi=hapus' }" class="btn btn-danger">Hapus</a>
			</td>
		</tr>
    </tbody>
    <?php
  }
  ?>
</table>
<!-- modal input -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Menu Baru</h4>
			</div>
			<div class="modal-body">
				<form action="proses.php?aksi=tambah" method="post">
					<div class="form-group">
						<label>Nama Menu</label>
						<input name="nama_menu" type="text" class="form-control" placeholder="Menu">
					</div>
					<div class="form-group">
						<label>Nama Kategori</label>
						<input name="nama_kategori" type="text" class="form-control" placeholder="Kategori">
					</div>
					<div class="form-group">
						<label>Foto</label>
						<input name="photos" type="text" class="form-control" placeholder="Upload foto">
					</div>
					<div class="form-group">
						<label>Harga</label>
						<input name="harga" type="text-area" class="form-control" placeholder="Alamat">
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>
