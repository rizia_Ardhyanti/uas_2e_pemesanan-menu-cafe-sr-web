<?php include 'header.php'; ?>
<?php
include 'koneksi.php';
$db = new database(); 
?>

<h3><span class="glyphicon glyphicon-user"></span>  Kategori Menu</h3>
<button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span>Tambah </button>
<br/>
<br/>
<div>
	<a style="margin-bottom:10px" href="" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
</div>
<form action="cari_act.php" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari user di sini .." aria-describedby="basic-addon1" name="cari">	
	</div>
</form>
<br/>
<table class="table table-hover">
    <thead>
  <tr>
		<th>No</th>
		<th>Nama Kategori</th>
		<th>Tindakan</th>
  </tr>
  </thead>
  <tbody>
  <?php
   $no = 1;
  foreach ($db->tampil_kategori() as $data) { ?>
   <tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['nama_kategori']; ?></td>
			<td>
				<a href="kategori_edit.php?id=<?php echo $data['id_kategori']; ?>&aksi=edit2" class="btn btn-warning">Edit</a>
				<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='proses.php?id_prodi=<?php echo $data['id_kategori']; ?>&aksi=hapus2' }" class="btn btn-danger">Hapus</a>
			</td>
		</tr>
    </tbody>
    <?php
  }
  ?>
</table>
<!-- modal input -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Kategori</h4>
			</div>
			<div class="modal-body">
				<form action="proses.php?aksi=tambah2" method="post">
					<div class="form-group">
						<label>Kategori Menu</label>
						<input name="nama_kategori" type="text" class="form-control" placeholder="Nama Kategori">
					</div>												
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>

